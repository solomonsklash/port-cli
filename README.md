# port-cli
### A command-line port number lookup utility, written in Python.


Potential usage: port-cli [port] tcp|udp|both|any -v 

- port-cli 22 tcp -v
	- ssh,22,tcp,The Secure Shell (SSH) Protocol,[RFC4251],
- port-cli 22 tcp
	- ssh,22,tcp,
- port-cli 22 udp -v
	- ssh,22,udp,The Secure Shell (SSH) Protocol,[RFC4251],
- port-cli 22 udp
	- ssh,22,udp,


**Output formatting (full verbosity indicated with *):**

	Port: 22
	Protocol: TCP
	Service: ssh
	Description: The Secure Shell (SSH) Protocol*
	RFC: RFC4251*

**Todo**
- Allow port number only 
- Account for unassigned port numbers.
- Add verbose mode that gives all listed info
- Capitalize protocol abbreviations.
- Return all protocols for a given port
- Make verbose default?
- Port 1000
